#include <Arduino.h>
#include "TimeTrigger.h"

TimeTrigger T100ms(50);

void setup() {
  Serial.begin(115200);

}

void loop() {
  if (T100ms.Trigger()){
    Serial.println(millis());

  }
}