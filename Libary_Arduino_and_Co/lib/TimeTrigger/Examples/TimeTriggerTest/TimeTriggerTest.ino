    /*
    This file is part of TimeTrigger Library
    TimeTrigger Libary is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TimeTrigger Libary is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TimeTrigger Libary.  If not, see <http://www.gnu.org/licenses/>.

    Diese Datei ist Teil von Fubar.

    Fubar ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.*/
	
#include <Arduino.h>
#include <TimeTrigger.h>

TimeTrigger Trigger100ms(100);
TimeTrigger Trigger1s(1000);

void setup() {
Serial.begin(115200);

  // put your setup code here, to run once:
}

void loop() {
  if (Trigger100ms.Trigger()){
    Serial.println(millis());}
  if (Trigger1s.Trigger()){
    Serial.println("jede Sekunde");
  }
}