#include <Arduino.h>
#include "TimeTrigger.h"

TimeTrigger::TimeTrigger(long TimesS)
{
    TimeSpann = TimesS;
    LastTime = millis();
}

TimeTrigger::~TimeTrigger()
{
}

bool TimeTrigger::Trigger()
{

    long now = millis();

    if (now - LastTime >= TimeSpann)
    {
        LastTime = now;
        return true;
    }
    else
    {
        return false;
    }
    
}

    void TimeTrigger::setTimeSpann(long Time1S)
    {
      TimeSpann = Time1S;

    }
