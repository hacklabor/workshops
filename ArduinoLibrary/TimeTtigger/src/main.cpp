#include <Arduino.h>
#include "TimeTrigger.h"

TimeTrigger Trigger100ms(100);
TimeTrigger Trigger1s(1000);

void setup() {
Serial.begin(115200);

  // put your setup code here, to run once:
}

void loop() {
  if (Trigger100ms.Trigger()){
    Serial.println(millis());}
  if (Trigger1s.Trigger()){
    Serial.println("jede Sekunde");
  }
}