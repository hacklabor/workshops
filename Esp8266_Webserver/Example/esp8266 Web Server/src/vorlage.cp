#include <arduino.h>
#include <ESP8266WiFi.h>
#include <FS.h>
#include <SerialDebug.h>
#include <FileSystem.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <secrets_test.h>

FileSystem fileSys(DEBUG);
SerialDebug DebugMain(DEBUG);
AsyncWebServer server(80);

String TXDATA[20];
String RXDATA[20];

String getRandom()
{
  return String(random(1, 100));
}

String processor(const String &var)
{
  DebugMain.print("processor Trigger Variable", var, DEBUG);
  if (var == "RANDOM1")
  {
    return getRandom();
  }
  if (var == "RANDOM2")
  {
    return getRandom();
  }

  return "";
}

void SERVER()
{
  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("IndexHTML", "LOAD", DEBUG);
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  // Route to load style.css file
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("Style.css", "LOAD", DEBUG);
    request->send(SPIFFS, "/style.css", "text/css");
  });
  server.on("/script.js", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("script.js", "LOAD", DEBUG);
    request->send(SPIFFS, "/script.js", "text/javascript");
  });
  server.on("/favicon.ico", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("favicon.ico", "LOAD", DEBUG);
    request->send(SPIFFS, "favicon.ico", "text/plain");
  });
  server.on("/newRandom", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("newRandom", "LOAD", DEBUG);
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  server.on("/newRandom1", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("newRandom1", "LOAD", DEBUG);
    request->send_P(200, "text/plain", getRandom().c_str());
  });
  server.on("/newRandom2*", HTTP_GET, [](AsyncWebServerRequest *request) {
    DebugMain.print("newRandom2", "LOAD", DEBUG);
    request->send_P(200, "text/plain", getRandom().c_str());
  });
  server.on(
      "/data", HTTP_POST, [](AsyncWebServerRequest *request) {}, NULL,
      [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
        String rxdat = "";
        for (size_t i = 0; i < len; i++)
        { 
          rxdat += char(data[i]);
         }
         DebugMain.print("POST DATA", rxdat, DEBUG);
        request->send_P(200, "text/plain", rxdat.c_str());
      });

  // Start server
  server.begin();
}

void setup(void)
{
  Serial.begin(115200);
  Serial.println();
  Serial.println();
  fileSys.mount();
   fileSys.WriteNewFile((char *)"settings", (char *)"da");
 fileSys.AppendTextToFile ((char *)"settings", (char *)"dateidatecsavwsevbewbwebswebwebwerbn");
   String TXT = fileSys.ReadFile((char *)"settings");
   DebugMain.print("Settings DATA", TXT, DEBUG);
  WiFi.begin(SSID, PASSWORT);
  WiFi.hostname(HOSTNAME);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());
  SERVER();
}

void loop()
{
  delay(500);
}