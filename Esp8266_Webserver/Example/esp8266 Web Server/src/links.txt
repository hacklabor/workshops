
Esp8266

- Flash Größe lesen Beispiel  https://github.com/esp8266/Arduino/blob/master/libraries/esp8266/examples/CheckFlashConfig/CheckFlashConfig.ino

Spiffs

- Dokumentation https://arduino-esp8266.readthedocs.io/en/latest/filesystem.html
- Partionen ld für PlatformIO Config https://github.com/esp8266/Arduino/tree/master/tools/sdk/ld
- Write File https://techtutorialsx.com/2019/05/28/esp8266-spiffs-writing-a-file/
- Read File https://techtutorialsx.com/2018/08/05/esp32-arduino-spiffs-reading-a-file/
- Append File https://techtutorialsx.com/2018/08/13/esp32-arduino-spiffs-append-content-to-file/


asynchroner Webserver

- https://randomnerdtutorials.com/esp8266-web-server-spiffs-nodemcu/
- https://randomnerdtutorials.com/esp32-esp8266-plot-chart-web-server/
- https://techtutorialsx.com/2018/10/12/esp32-http-web-server-handling-body-data/

Biblotheken

- https://github.com/me-no-dev/ESPAsyncWebServer.git
- https://github.com/me-no-dev/ESPcAsyncTCP.git
- https://github.com/Th0m4sK/SerialDebug.git
- https://github.com/Th0m4sK/FileSystem.git

