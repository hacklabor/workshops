#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include <FS.h>
#include "secrets_test.h"
AsyncWebServer server(80);



void setup()
{
  Serial.begin(115200); //Open Serial connection

  WiFi.begin(SSID, PASSWORT); //Connect to the WiFi network
  WiFi.hostname(HOSTNAME);
  while (WiFi.status() != WL_CONNECTED)
  { //Wait for connection

    delay(500);
    Serial.println("Waiting to connect…");
  }
  IPAddress Ip = WiFi.localIP();
  Serial.println(Ip.toString());
}

void loop()
{
  // put your main code here, to run repeatedly:
}