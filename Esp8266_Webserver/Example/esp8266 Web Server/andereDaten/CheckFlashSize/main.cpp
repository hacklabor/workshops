#include <arduino.h>
#include <FS.h>

/*
  ESP8266 CheckFlashConfig by Markus Sattler
  This sketch tests if the EEPROM settings of the IDE match to the Hardware
  https://github.com/esp8266/Arduino/blob/master/libraries/esp8266/examples/CheckFlashConfig/CheckFlashConfig.ino

*/

FSInfo fs_info;

void setup(void)
{
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  
  if (SPIFFS.begin())
  {
    Serial.println("SPIFFS Flash found");
    if (SPIFFS.info(fs_info))
    {
     Serial.println("max Size : " +
                     String(fs_info.totalBytes) + 
                     " Bytes-- > used : " + 
                     String(fs_info.usedBytes) + 
                     " Bytes ");
    }
  }
  else
  {
    Serial.println("SPIFFS not found");
  }

 


  Serial.println("----------------------------------");

  uint32_t realSize = ESP.getFlashChipRealSize();

  uint32_t ideSize = ESP.getFlashChipSize();
  FlashMode_t ideMode = ESP.getFlashChipMode();

  Serial.printf("Flash real id:   %08X\n", ESP.getFlashChipId());
  Serial.printf("Flash real size: %u bytes\n\n", realSize);

  Serial.printf("Flash ide  size: %u bytes\n", ideSize);
  Serial.printf("Flash ide speed: %u Hz\n", ESP.getFlashChipSpeed());
  Serial.printf("Flash ide mode:  %s\n", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));

  if (ideSize != realSize)
  {
    Serial.println("Flash Chip configuration wrong!\n");
  }
  else
  {
    Serial.println("Flash Chip configuration ok.\n");
  }
}

void loop()
{

  delay(5000);
}